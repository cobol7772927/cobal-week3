       IDENTIFICATION DIVISION.
       PROGRAM-ID. PRO1.
       AUTHOR. KITTIPON.
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 NUM1  PIC   99.
       01 NUM2  PIC   99.
       01 NUM3  PIC   99.
       01 NUM4  PIC   99.

       PROCEDURE DIVISION .
           PERFORM PROBLEM1.
           PERFORM PROBLEM2.
           PERFORM PROBLEM3.
           PERFORM PROBLEM4.
           PERFORM PROBLEM5.
           PERFORM PROBLEM6.
           PERFORM PROBLEM7.
           PERFORM PROBLEM8.
           PERFORM PROBLEM9.
           PERFORM PROBLEM10.
           PERFORM PROBLEM11.
           GOBACK.
       HEADER.
           DISPLAY "********************************************".
           EXIT.
       DISPLAY-BEFORE.
           DISPLAY "BEFORE : " NUM1 " " NUM2 " " NUM3 " " NUM4
           EXIT.
       DISPLAY-AFTER.
           DISPLAY "AFTER  : " NUM1 " " NUM2 " " NUM3 " " NUM4
           EXIT.
       PROBLEM1.
           PERFORM HEADER.
           DISPLAY "PROBLEM 1 : ADD NUM1 TO NUM2".
           MOVE 25 TO NUM1 
           MOVE 30 TO NUM2 
           MOVE ZERO TO NUM3 
           MOVE ZERO TO NUM4 
           PERFORM DISPLAY-BEFORE 
           ADD NUM1 TO NUM2 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM2.
           PERFORM HEADER.
           DISPLAY "PROBLEM 2 : ADD NUM1, NUM2 TO NUM3, NUM4".
           MOVE 13 TO NUM1 
           MOVE 04 TO NUM2 
           MOVE 05 TO NUM3 
           MOVE 12 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           ADD NUM1, NUM2 TO NUM3, NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM3.
           PERFORM HEADER.
           DISPLAY "PROBLEM 3 : ADD NUM1, NUM2, NUM3 GIVING NUM4".
           MOVE 04 TO NUM1 
           MOVE 03 TO NUM2 
           MOVE 02 TO NUM3 
           MOVE 01 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           ADD NUM1, NUM2, NUM3 GIVING NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM4.
           PERFORM HEADER.
           DISPLAY "PROBLEM 4 : SUBTRACT NUM1 FROM NUM2 GIVING NUM3 ".
           MOVE 04 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1 FROM NUM2 GIVING NUM3 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM5.
           PERFORM HEADER.
           DISPLAY "PROBLEM 5 : SUBTRACT NUM1, NUM2 FROM NUM3".
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1, NUM2 FROM NUM3 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM6.
           PERFORM HEADER.
           DISPLAY "PROBLEM 6 : SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM
      -     "4".
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 55 TO NUM3 
           MOVE 20 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           SUBTRACT NUM1, NUM2 FROM NUM3 GIVING NUM4  
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM7.
           PERFORM HEADER.
           DISPLAY "PROBLEM 7 : MULTIPLY NUM1 BY NUM2"
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           MULTIPLY NUM1 BY NUM2 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM8.
           PERFORM HEADER.
           DISPLAY "PROBLEM 8 : MULTIPLY NUM1 BY NUM2 GIVING NUM3"
           MOVE 05 TO NUM1 
           MOVE 10 TO NUM2 
           MOVE 33 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           MULTIPLY NUM1 BY NUM2 GIVING NUM3 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM9.
           PERFORM HEADER.
           DISPLAY "PROBLEM 9 : DIVIDE NUM1 INTO NUM2"
           MOVE 05 TO NUM1 
           MOVE 64 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           DIVIDE NUM1 INTO NUM2 
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM10.
           PERFORM HEADER.
           DISPLAY "PROBLEM 10 : DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAIND
      -    "ER NUM4"
           MOVE 05 TO NUM1 
           MOVE 64 TO NUM2 
           MOVE 24 TO NUM3 
           MOVE 88 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           DIVIDE NUM2 BY NUM1 GIVING NUM3 REMAINDER NUM4
           PERFORM DISPLAY-AFTER 
           EXIT.
       PROBLEM11.
           PERFORM HEADER.
           DISPLAY "PROBLEM 11 : COMPUTE NUM1 = 5 + 10 * 30 / 2"
           MOVE 25 TO NUM1 
           MOVE 00 TO NUM2 
           MOVE 00 TO NUM3 
           MOVE 00 TO NUM4 
           PERFORM DISPLAY-BEFORE 
           COMPUTE NUM1 = 5 + 10 * 30 / 2
           PERFORM DISPLAY-AFTER 
           EXIT.